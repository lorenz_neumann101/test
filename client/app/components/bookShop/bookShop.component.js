import template from './bookShop.html';
import controller from './bookShop.controller';
import './bookShop.scss';

let bookShopComponent = {
  bindings: {},
  template,
  controller
};

export default bookShopComponent;
