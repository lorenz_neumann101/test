import BookShopModule from './bookShop';
import BookShopController from './bookShop.controller';
import BookShopComponent from './bookShop.component';
import BookShopTemplate from './bookShop.html';

describe('BookShop', () => {
  let $rootScope, makeController;

  beforeEach(window.module(BookShopModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new BookShopController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(BookShopTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = BookShopComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(BookShopTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(BookShopController);
    });
  });
});
