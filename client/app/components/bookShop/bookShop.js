import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bookShopComponent from './bookShop.component';

let bookShopModule = angular.module('bookShop', [
  uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
  "ngInject";

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('home', {
      url: '/',
      component: 'bookShop'
    });
})

.component('bookShop', bookShopComponent)

.name;

export default bookShopModule;
