import template from './eBooks.html';
import controller from './eBooks.controller';
import './eBooks.scss';

let eBooksComponent = {
  bindings: {},
  template,
  controller
};

export default eBooksComponent;
