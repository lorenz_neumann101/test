import EBooksModule from './eBooks';
import EBooksController from './eBooks.controller';
import EBooksComponent from './eBooks.component';
import EBooksTemplate from './eBooks.html';

describe('EBooks', () => {
  let $rootScope, makeController;

  beforeEach(window.module(EBooksModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new EBooksController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(EBooksTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = EBooksComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(EBooksTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(EBooksController);
    });
  });
});
