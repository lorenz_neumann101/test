import angular from 'angular';
import uiRouter from 'angular-ui-router';
import eBooksComponent from './eBooks.component';

let eBooksModule = angular.module('eBooks', [
  uiRouter
])

.component('eBooks', eBooksComponent)

.name;

export default eBooksModule;
