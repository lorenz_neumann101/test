class EBooksController {
  constructor($http) {
    this.name = 'eBooks';
    this.eBooks = [];
    this.$http = $http;
  }
  
  $onInit() {
    this.$http.get('app/components/eBooks/eBooks.json')
      .then((response) => {
        // console.log(response)
        this.eBooks = response.data;
      })
  }
}

EBooksController.$inject = ['$http'];
export default EBooksController;
