class CommentsController {
  constructor() {
    this.name = 'comments';
    this.comments = [];
  }

  $onInit() {
    this.comments = [
      {
        "username": "Cool user 1",
        "content": "This is very nice",
        "timestamp": "01.06.2017 09:22"
      },
      {
        "username": "Cool user 2",
        "content": "Awesome!",
        "timestamp": "01.06.2017 11:12"
      },
      {
        "username": "Cool user 3",
        "content": "I don't like this",
        "timestamp": "03.06.2017 13:16"
      },
      {
        "username": "Cool user 4",
        "content": "Good job!",
        "timestamp": "05.06.2017 01:45"
      },
      {
        "username": "Cool user 5",
        "content": "oh no...",
        "timestamp": "05.06.2017 13:24"
      },
      {
        "username": "Cool user 6",
        "content": "Yeah..!",
        "timestamp": "05.06.2017 21:12"
      },
      {
        "username": "Cool user 7",
        "content": "I shared it.",
        "timestamp": "06.06.2017 11:12"
      },
      {
        "username": "Cool user 8",
        "content": "I love cats",
        "timestamp": "07.06.2017 22:12"
      },
      {
        "username": "Cool user 9",
        "content": "Good job",
        "timestamp": "08.06.2017 11:12"
      },
      {
        "username": "Cool user 10",
        "content": ":)",
        "timestamp": "08.06.2017 23:12"
      }
    ];
  }
}

export default CommentsController;
