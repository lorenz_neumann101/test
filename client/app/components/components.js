import angular from 'angular';
import Home from './home/home';
import About from './about/about';
import Comments from './comments/comments';
import EBooks from './eBooks/eBooks';
import BookShop from './bookShop/bookShop';

let componentModule = angular.module('app.components', [
  Home,
  About,
  Comments,
  EBooks,
  BookShop
])

.name;

export default componentModule;
